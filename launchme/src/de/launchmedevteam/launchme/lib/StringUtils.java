package de.launchmedevteam.launchme.lib;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
public class StringUtils {
    public static String getStringBetween(String base, String begin, String end) {

        Pattern patbeg = Pattern.compile(Pattern.quote(begin));
        Pattern patend = Pattern.compile(Pattern.quote(end));

        int resbeg = 0;
        int resend = base.length() - 1;

        Matcher matbeg = patbeg.matcher(base);

        if (matbeg.find())
            resbeg = matbeg.end();

        Matcher matend = patend.matcher(base);

        if (matend.find())
            resend = matend.start();

        return base.substring(resbeg, resend);
    }
}
