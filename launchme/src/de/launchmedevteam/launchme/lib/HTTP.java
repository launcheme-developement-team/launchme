package de.launchmedevteam.launchme.lib;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import de.launchmedevteam.launchme.classes.HTTPResponse;
import de.launchmedevteam.launchme.main.Main;

public class HTTP {
	public static HTTPResponse Post_JSON(JSONObject json, URL url) {
		HTTPResponse response = null;
		HttpURLConnection conn = null;
		try {
			if (Main.debug)
				disableSSLCertificateChecking();
			// Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("127.0.0.1",
			// 8888));
			conn = (HttpURLConnection) url.openConnection(/* proxy */);
			conn.setConnectTimeout(1000);
			conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			conn.setRequestProperty("Accept", "application/json, text/plain");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("POST");
			OutputStream os = conn.getOutputStream();
			os.write(json.toString().getBytes("UTF-8"));
			os.close();
			// read the response
			InputStream in = new BufferedInputStream(conn.getInputStream());
			String result = IOUtils.toString(in, "UTF-8");
			response = new HTTPResponse(result, conn.getResponseCode());
			in.close();
			conn.disconnect();
		} catch (IOException e) {
			try {
				if (conn != null)
					response = new HTTPResponse(conn.getResponseMessage(), conn.getResponseCode());
			} catch (IOException e1) {
				response = new HTTPResponse("", 0);
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return response;
	}

	private static void disableSSLCertificateChecking() {
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			@Override
			public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
				// Not implemented
			}

			@Override
			public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
				// Not implemented
			}
		} };

		try {
			SSLContext sc = SSLContext.getInstance("TLS");

			sc.init(null, trustAllCerts, new java.security.SecureRandom());

			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
}
