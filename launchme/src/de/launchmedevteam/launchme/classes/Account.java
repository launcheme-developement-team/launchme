package de.launchmedevteam.launchme.classes;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.launchmedevteam.launchme.lib.HTTP;
import de.launchmedevteam.launchme.lib.UUIDTools;
import de.launchmedevteam.launchme.main.Logger;
import de.launchmedevteam.launchme.main.Main;

public class Account {
	UUID account_uuid; // UUID of mojang Account
	String account_username; // Username of mojang Account
	UUID uuid; // UUID of minecraft Account
	String token;
	boolean valid = false;
	String username;
	boolean checked = false;
	boolean changed = false;

	public String getAccountUsername() {
		return account_username;
	}

	public Account(String token, UUID uuid, String username, UUID account_uuid, String account_username,
			boolean verify) {

		boolean setinfos = false;
		if (verify) {
			verify();
		} else {
			setinfos = true;
			this.checked = false;

		}

		if (setinfos) {
			this.valid = true;
			this.uuid = uuid;
			this.token = token;
			this.username = username;
			this.account_uuid = account_uuid;
			this.account_username = account_username;

		}

	}

	public Account(String username, String password) {
		Logger.log(4, username);
		JSONObject payload = new JSONObject();

		try {
			// More Information: https://wiki.vg/Authentication
			payload.put("username", username);
			payload.put("password", password);
			payload.put("clientToken", Main.settings.getClientToken().toString().replace("-", ""));
			payload.put("requestUser", true);

			// Agent

			payload.put("agent", new JSONObject().put("name", "Minecraft").put("version", 1));

			HTTPResponse httpresponse = null;
			try {
				httpresponse = HTTP.Post_JSON(payload, new URL("https://authserver.mojang.com/authenticate"));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			if (httpresponse != null) {
				switch (httpresponse.getResponseCode()) {
				case 0:
					Logger.log(5, "No networkconnection! Offlinemode...");
					Main.offlinemode = true;
					break;
				case 200:
					// All ok
					Logger.log(5, "Auth success");
					setInformations(httpresponse.getResponseJSON());
					this.changed = true;
					Main.settings.changed = true;
					this.valid = true;
					break;
				case 403:
					// Username/Password/... wrong
					Logger.log(5, "Auth wrong");
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public UUID getUUID() {
		return uuid;
	}

	public String getAccessToken() {
		return token;
	}

	public String getUserName() {
		return username;
	}


	private void setInformations(JSONObject response) {
		try {
			if (!response.has("clientToken")
					&& UUIDTools.fromTrimmed(response.getString("clientToken")) != Main.settings.getClientToken())
				return;
			if (response.has("accessToken"))
				token = response.getString("accessToken");
			else
				return;
			if (response.has("user")) {
				JSONObject user = response.getJSONObject("user");
				if (!user.has("blocked") && !user.has("suspended") && user.getBoolean("blocked")
						&& user.getBoolean("suspended"))
					return;
				account_username = user.getString("username");
				account_uuid = UUIDTools.fromTrimmed(user.getString("id"));
			} else
				return;
			if (response.has("availableProfiles")) {
				JSONArray profiles = response.getJSONArray("availableProfiles");
				for (int i = 0; i < profiles.length(); i++) {
					JSONObject profile = profiles.getJSONObject(i);
					if (!profile.has("agent") && !profile.getString("agent").equalsIgnoreCase("minecraft"))
						break;

					if (!profile.has("paid") && !profile.getBoolean("paid")) {
						// Minecraft not paid. Only demo. Demo = unsupported!
						break;
					}
					if (!profile.has("suspended") && profile.getBoolean("suspended")) {
						// Account suspended
						break;
					}
					if (profile.has("id") && profile.has("name")) {
						uuid = UUIDTools.fromTrimmed(profile.getString("id"));
						username = profile.getString("name");
						Logger.log(5, "Login: " + uuid + ": " + username);
						// Everything correct. Add it!
					} else
						return;

				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private String refreshToken(String token) {

		JSONObject payload = new JSONObject();

		try {
			// More Information: https://wiki.vg/Authentication
			payload.put("clientToken", Main.settings.getClientToken().toString().replace("-", ""));
			payload.put("accessToken", token);
			payload.put("requestUser", true);

			HTTPResponse httpresponse = null;
			try {
				httpresponse = HTTP.Post_JSON(payload, new URL("https://authserver.mojang.com/refresh"));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			if (httpresponse != null) {
				switch (httpresponse.getResponseCode()) {
				case 200:
					// All ok
					Logger.log(5, "Refresh success");
					this.changed = true;
					Main.settings.changed = true;
					// returns new token
					return httpresponse.getResponseJSON().getString("accessToken");
				case 403:
					// token still wrong!
					return null;
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return null;
	}

	public boolean verify() {
		JSONObject payload = new JSONObject();
		try {
			// More Information: https://wiki.vg/Authentication
			payload.put("accessToken", token);
			payload.put("clientToken", Main.settings.getClientToken().toString().replace("-", ""));
			// payload.put("requestUser", true);

			HTTPResponse httpresponse = null;
			try {
				httpresponse = HTTP.Post_JSON(payload, new URL("https://authserver.mojang.com/validate"));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			if (httpresponse != null) {
				switch (httpresponse.getResponseCode()) {
				case 0:
					Logger.log(5, "No networkconnection! Offlinemode...");
					Main.offlinemode = true;
					break;
				case 204:
					// All ok
					Logger.log(5, "Auth success");
					this.valid = true;
					return true;
				case 403:
					// token... wrong
					Logger.log(5, "Refreshing Token: " + account_username);
					token = refreshToken(token);
					if (token == null)
						Logger.log(6, "Refresh failed!");
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		this.checked = true;
		return false;
	}

	public boolean isValid() {
		return valid;
	}

	public UUID getAccountUUID() {
		return account_uuid;
	}

	public JSONObject generateJSON() {

		return null;
	}

	public String getAccountType() {
		//mojang or minecraft
		return "mojang";
	}
}
