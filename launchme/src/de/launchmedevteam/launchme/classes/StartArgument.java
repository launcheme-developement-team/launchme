package de.launchmedevteam.launchme.classes;

public class StartArgument {
	private int name;
	private Object value;
	public static final int DEBUG = 1 /* chars */;

	public StartArgument(String text) {
		String raw[] = text.substring(1, text.length())/* Substing - */.split("=", 2);
		switch (raw[0]) {
		case "debug":
			name = DEBUG;
			break;
		}
		value = raw[1];
	}
	public int getType() {
		return name;
	}
	public Object getValue() {
		return value;
	}

}
