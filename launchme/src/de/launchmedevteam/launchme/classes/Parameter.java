package de.launchmedevteam.launchme.classes;

import de.launchmedevteam.launchme.main.Logger;

public class Parameter {
	String delimiter;
	String name;
	String value;
	boolean stringchar = true;
	public Parameter(String name, String value) {
		this.name = name;
		this.value = value;
		
	}
	public Parameter(String name, String value, String delimiter) {
		this.name = name;
		this.value = value;
		this.delimiter = delimiter;
	}
	public Parameter(String name, String value, String delimiter, boolean stringchar) {
		this.name = name;
		this.value = value;
		this.delimiter = delimiter;
		this.stringchar = stringchar;
	}

	public void pushValue(String value, String delimiter) {
		if(!this.value.isEmpty())
			this.value += delimiter;
		this.value += value;		
	}
	public String getName() {
		
		return name;
	}
	public String getValue() {
		if(value == null)
			return "";
		return value;
	}
	public void setValue(String value) {
		this.value = value;
		
	}
	public String getDelimiter() {
		if(value == null)
			return "";
		if(delimiter == null)
			return "=";
		
		return delimiter;
	}
	public boolean needStringChar() {
		if(value == null)
			return false;
		if(value.contains("\\ ")) {
			Logger.log(4, "Need to add \", becuase of space char");
			return true;
		}
		return stringchar;
	}

}
