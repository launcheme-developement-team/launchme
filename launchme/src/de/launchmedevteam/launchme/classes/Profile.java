package de.launchmedevteam.launchme.classes;

import java.util.ArrayList;

public class Profile {
	Version v;
	Resolution r;
	ArrayList<Parameter> jvm_params;
	String name;

	public Profile(String name, Version v, Resolution r, ArrayList<Parameter> params) {
		this.v = v;
		this.jvm_params = params;
		this.name = name;
		this.r = r;
	}

	public ArrayList<Parameter> getJVMAgrs() {
		return jvm_params;
	}

	public Version getVersion() {
		return v;
	}

	public String getName() {
		return name;
	}
	
	public void setVersion(Version v) {
		this.v = v;
	}

	public Resolution getResolution() {
		return r;
	}
}
