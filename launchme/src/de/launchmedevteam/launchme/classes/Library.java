package de.launchmedevteam.launchme.classes;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;

import de.launchmedevteam.launchme.lib.Hash;
import de.launchmedevteam.launchme.lib.ZIP;
import de.launchmedevteam.launchme.main.Logger;
import de.launchmedevteam.launchme.main.Main;

public class Library {
	boolean type = false; // 0=lib; 1=classifier
	String version;
	String name;
	String path;
	String sha1;
	int size;
	URL url;
	boolean downloaded = false; // true if alredy downloaded or download failed!
	
	UUID tempdir;
	
	boolean verified = false;

	public String getRelativePath() {
		return "";
	}

	public Library(JSONObject lib, UUID tempdir) throws JSONException {

		this.tempdir = tempdir;
		// tempdir: Extract libs from classifiers

		System.out.println("Trying add lib: " + lib.getString("name"));
		if (lib.has("downloads")) {
			// No classifier
			boolean allowedToAdd = true;
			if (lib.has("rules")) {
				allowedToAdd = false;
				boolean generalRule = true;
				for (int iii = 0; iii < lib.getJSONArray("rules").length(); iii++) {

					JSONObject rule = lib.getJSONArray("rules").getJSONObject(iii);
					System.out.println("Rule:" + rule.toString());
					boolean action = false;
					if (rule.getString("action") == "allow") 
						action = true;

					if (rule.has("os")) {

						if(generalRule == false) {
							break;
							
						}
						allowedToAdd = action;
						generalRule = false;
						System.out.println("OS definition");
						if (rule.getJSONObject("os").getString("name").contains(Main.os)) 
							allowedToAdd = action;
					} else 
						allowedToAdd = action;

				}

			}
			if (allowedToAdd)
				System.out.println("Allowed to add!");
			else
				System.out.println("Disallowed to add!");

			if (allowedToAdd) {
				JSONObject data = null;
				if (lib.getJSONObject("downloads").has("artifact")) {
					data = lib.getJSONObject("downloads").getJSONObject("artifact");

				}

				if (lib.getJSONObject("downloads").has("classifiers")) {
					System.out.println("[LIB] classifiers found....");
					String arch = "";
					if (lib.has("natives")) {
						arch = lib.getJSONObject("natives").getString(Main.os);
						arch = arch.replace("${arch}", System.getProperty("sun.arch.data.model"));
						System.out.println(arch);
						if (lib.getJSONObject("downloads").getJSONObject("classifiers").has(arch)) {
							type = true;
							data = lib.getJSONObject("downloads").getJSONObject("classifiers").getJSONObject(arch);

						}
					}

				}
				if (data.has("path")) {
					path = data.getString("path");
				}
				if (data.has("sha1")) {
					sha1 = data.getString("sha1");
				}
				if (data.has("path")) {
					size = data.getInt("size");
				}
				if (data.has("path")) {
					try {
						url = new URL(data.getString("url"));
					} catch (MalformedURLException e) {
						Logger.log(5, "Unvailid URL!");
						e.printStackTrace();
					}
				}

			}

		} else {
			if (lib.has("name")) {
				// Maybe lib is correct...Checking
				System.out.println("[WARN] Maybe this is an error!");
				String[] nametopath = lib.getString("name").substring(0, lib.getString("name").indexOf(":"))
						.split("\\.");
				path = "";
				for (int iiii = 0; iiii < nametopath.length; iiii++) {
					path += nametopath[iiii] + "/";
				}

				String[] filetopath = lib.getString("name")
						.substring(lib.getString("name").indexOf(":"), lib.getString("name").length()).split(":");

				path += filetopath[1] + "/" + filetopath[2] + "/" + filetopath[1] + "-" + filetopath[2] + ".jar";

			}

		}
		verify();

	}

	private void verify() {
		if (path != null) {
			File f = new File(getInteralAbsoluFilePath());
			if (f.exists() && f.isFile()) {
				boolean skip = false;
				if (size != 0 && f.length() != size) {
					skip = true;
					Logger.log(6, "File Size wrong!");
				}
				if (!skip) {
					try {
						if (sha1 != null && !sha1.equalsIgnoreCase(Hash.createSha1(f))) {
							skip = true;
							Logger.log(6, "Hash wrong!");
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				if (skip) {
					if (!Main.offlinemode && !downloaded) {
						download();
						verify();
					}
					return;
				}

				if (type) {
						System.out.println("Extracting...");
						ZIP.unZipIt(getInteralAbsoluFilePath(), System.getProperty("java.io.tmpdir") + "/launchme/minecraft/" + tempdir.toString());
					}
					verified = true;

				} else
					System.out.println("[WARN] Lib does not exists(" + path + ")");
			}
		}

	private void download() {
		// Download from Server
		// must reset size,....
		downloaded = true;
	}
	

	private String getInteralAbsoluFilePath() {
		return Main.settings.minecraft_path + "global/downloads/libraries/" + path;
		
	}
	public String getAbsolutFilePath() {
		if(path == null || type || path == "")
			return null;
		return Main.settings.minecraft_path + "global/downloads/libraries/" + path;
	}

}
