package de.launchmedevteam.launchme.classes;

import java.util.ArrayList;
import java.util.UUID;

import de.launchmedevteam.launchme.main.Logger;
import de.launchmedevteam.launchme.main.Main;
import de.launchmedevteam.launchme.main.MinecraftProcessBuilder;

public class MinecraftInstance {
	MinecraftProcessBuilder process;
	Account a;
	Group g;
	Profile p;
	UUID tempdir;
	public ArrayList<Parameter> additional_params = new ArrayList<>();

	public MinecraftInstance(Profile p, Account a, Group g, UUID temp) {
		if (Main.offlinemode) {
			// Account (valid)
			this.a = a;
		} else if (a.verify()) {
			// Account valid
			this.a = a;
		} else {
			Logger.log(4, "Account invalid!");
			this.a = a;
			// Account invalid!
		}
		Version mod_v = p.getVersion();
		mod_v.setTempDir(temp);
		p.setVersion(mod_v);
		this.p = p;
		this.g = g;
		this.tempdir = temp;
	}

	public MinecraftProcessBuilder start() {
		process = new MinecraftProcessBuilder(this);
		return process;
	}

	public ArrayList<Parameter> getJVMArgs() {
		return additional_params;
	}

	public Version getVersion() {
		return p.getVersion();
	}

	public ArrayList<Parameter> getAdditionalParameters() {
		return p.getJVMAgrs();

	}

	public Account getAccount() {
		return a;
	}

	public Group getGroup() {
		return g;
	}

	public Profile getProfile() {
		return p;
	}

	public UUID getTempDir() {
		return tempdir;
	}
}
