package de.launchmedevteam.launchme.classes;

import org.json.JSONException;
import org.json.JSONObject;

public class HTTPResponse {
	int responsecode;
	String response;
	String response_header;
	public HTTPResponse(String response, int responsecode,String response_header) {
		this.responsecode = responsecode;
		this.response = response;
		this.response_header = response_header;
	}
	public HTTPResponse(String response, int responsecode) {
		this.responsecode = responsecode;
		this.response = response;
	}
	
	public int getResponseCode() {
		return responsecode;
	}
	
	public String getResponse() {
		return response;
	}
	
	public JSONObject getResponseJSON() throws JSONException {
		return new JSONObject(response);
	}
	
	public String getResponseHeader() {
		return response_header;
	}
	
	
	
	
	

}
