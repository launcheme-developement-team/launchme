package de.launchmedevteam.launchme.classes;

import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.launchmedevteam.launchme.lib.UUIDTools;
import de.launchmedevteam.launchme.main.Logger;
import de.launchmedevteam.launchme.main.Main;

public class LauncherSettings { 

	JSONObject raw_settings;
	String minecraft_path = Main.application_data_path + "mc/";
	UUID client_token;
	Language language;
	boolean changed = false;
	JSONArray raw_errors = new JSONArray();
    public ArrayList<Account> accounts = new ArrayList<>();
    public int selected_account; //Array index(0-...)
    
    //Java Args
    int default_memory = 2048; //MB
	
	
	public LauncherSettings(JSONObject settings) {
		raw_settings = settings;
	}
	
	public boolean check() {
		JSONObject temp;
		try {
			temp = raw_settings.getJSONObject("settings");
			if(temp.has("path_of_minecraft")) {
				minecraft_path = temp.getString("path_of_minecraft");
				File f = new File(minecraft_path);
				if(!f.exists() || !f.isDirectory()) {
					Logger.log(8, "Minecraft Path does not exists. Creating it...");
					f.mkdirs();
				}
			}else {
				raw_errors.put(2);
			}
			if(temp.has("language")) {
				Main.lang = new Language(temp.getString("language"));
				if(!Main.lang.exists())
					Main.lang = new Language();
			}else {
				Logger.log(8, "No Language set! Setting to fallback language...");
				Main.lang = new Language();
				raw_errors.put(3);
			}
		} catch (JSONException e) {
			//settings not set!
			raw_errors.put(1);
			if(Main.debug)
				e.printStackTrace();
		}
		try {
			temp = raw_settings.getJSONObject("auth");
			if(temp.has("clientToken")) {
				client_token = UUIDTools.fromTrimmed(temp.getString("clientToken"));
			}else {
				//no Client token. set one!
				raw_errors.put(5);
			}
			if(temp.has("accounts")) {
				addAccounts(temp.getJSONArray("accounts"));
				//need to set selected one
				
			}
			
		} catch (JSONException e) {
			raw_errors.put(4);
			if(Main.debug)
			e.printStackTrace();
		}
		
		
		
		if(raw_errors.length() > 0) {

			Logger.log(4, "1");
			return false;
		}
		return true;
	}
	public JSONArray getErrors() {

		return raw_errors;
	}
	
	public void writeToFile() {
		
		
		
		
		
	}
	
	public String getMinecraftPath() {
		return minecraft_path;
		
	}
	
	public UUID getClientToken() {
		return client_token;
	}
	public void addAccounts(JSONArray raw) {
		for(int i = 0; i < raw.length(); i++) {
			try {
				JSONObject account = raw.getJSONObject(i);
				if(account.has("uuid")&&account.has("displayName")&&account.has("username")&&account.has("accessToken")&&account.has("id")) {
					Account acc = new Account(account.getString("accessToken"), UUIDTools.fromTrimmed(account.getString("uuid")),account.getString("displayName"),UUIDTools.fromTrimmed(account.getString("id")),account.getString("username"), false);
					
					//Add Account
					accounts.add(acc);
					
					
					Logger.log(5, "Account added: " + account.getString("username"));
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	public void setSelectedAccount(UUID u) {
		for(int i = 0; i < accounts.size(); i++) {
			Account a = accounts.get(i);
			if(a.getAccountUUID() == u)
				selected_account = i;
		}
	}
	
	public JSONObject generateRawJSON() {
		if(this.changed){

		JSONObject temp = new JSONObject();
		try {
			temp.put("versions", new JSONObject().put("version", 2).put("launchme_version", Main.version));
			temp.put("settings", new JSONObject().put("language", language.getCode()).put("path_of_minecraft", minecraft_path));
			JSONArray temp2 = new JSONArray();
			for(int i = 0; i < accounts.size(); i++){
				temp2.put(accounts.get(i).generateJSON());
			}
			temp.put("auth", new JSONObject().put("clientToken", this.client_token.toString()).put("selected", selected_account).put("accounts",temp2));
			//Still need to To: Export all groups/...
			changed  =false;
			raw_settings = temp;
			return temp;
		} catch (JSONException e) {
			if(Main.debug)
				e.printStackTrace();
		}
		}
		
		return null;
		
		
		
	}
	
	public int getDefaultRam() {
		return default_memory;
	}
}


