package de.launchmedevteam.launchme.classes;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.launchmedevteam.launchme.main.Logger;
import de.launchmedevteam.launchme.main.Main;

public class Version {
	private JSONObject rawDetails;
	private JSONObject rawInherit;
	UUID tempdir;

	public Version(JSONObject details) throws JSONException {
		Logger.log(4, "Adding Version...");
		rawDetails = details;
	}

	public void setTempDir(UUID dir) {
		tempdir = dir;
	}

	public JSONObject getRaw() {
		return rawDetails;
	}

	public ArrayList<Library> getLibs() {
		ArrayList<Library> libs = new ArrayList<Library>();
		try {
			JSONArray a = rawDetails.getJSONArray("libraries");
			for (int i = 0; i < a.length(); i++) {
				libs.add(new Library(a.getJSONObject(i), tempdir));

			}
		} catch (JSONException e) {
			Logger.log(5, "Version file corrupted!");
			e.printStackTrace();
		}
		return libs;
	}

	public ArrayList<Library> getInheritLibs() {
		ArrayList<Library> libs = new ArrayList<Library>();
		if (rawDetails.has("inheritsFrom")) {
			File inherit = null;
			try {
				inherit = new File(Main.settings.minecraft_path + "global/versions/"
						+ rawDetails.getString("inheritsFrom") + "/" + rawDetails.getString("inheritsFrom") + ".json");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			if (inherit != null && inherit.exists()) {
				try {
					rawInherit = new JSONObject(new String(Files.readAllBytes(Paths.get(inherit.getAbsolutePath()))));
				} catch (JSONException e) {
					Logger.log(6, "Invailid Inherit File!");
					e.printStackTrace();
				} catch (IOException e) {
					Logger.log(9, "Error!");
					e.printStackTrace();
				}
			} else {
				Logger.log(5, "Inherit Version does not exists! Downloading it...");
				// Now: Download Version
			}
			try {
				JSONArray a = rawInherit.getJSONArray("libraries");
				for (int i = 0; i < a.length(); i++) {
					libs.add(new Library(a.getJSONObject(i), tempdir));

				}
			} catch (JSONException e) {
				Logger.log(5, "Version file corrupted!");
				e.printStackTrace();
			}
		}
		return libs;
	}

	public ArrayList<Library> getAllLibs() {
		ArrayList<Library> libs = new ArrayList<Library>();
		libs.addAll(getLibs());
		libs.addAll(getInheritLibs());
		return libs;
	}


	public String getMainClass() {
		try {
			return rawDetails.getString("mainClass");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

	public String getMinecraftArguments() {
		try {
			return rawDetails.getString("minecraftArguments");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

	public String getType() {
		try {
			return rawDetails.getString("type");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "unknown";
	}

	public String getVersionName() {
		try {
			return rawDetails.getString("id");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "1.0";
	}

	public String getAssetsIndex() {
		try {
			return rawDetails.getString("assets");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "1.0";
	}
}
