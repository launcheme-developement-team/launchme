package de.launchmedevteam.launchme.classes;

import java.awt.Dimension;
import java.awt.Toolkit;

public class Resolution {
	public static boolean MAXIMIZED = true;
	public static boolean MINIMIZED = false;

	int width = 480;
	int height = 640;

	public Resolution(boolean type) {
		// Resolution = Max
		// type = MAXIMIZED/MINIMIZED
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		width = (int) screenSize.getWidth();
		height = (int) screenSize.getHeight();
	}

	public Resolution(int w, int h) {
		// Window size
		width = w;
		height = h;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

}
