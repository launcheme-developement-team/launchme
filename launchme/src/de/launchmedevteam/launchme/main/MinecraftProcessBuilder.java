package de.launchmedevteam.launchme.main;

import de.launchmedevteam.launchme.classes.Library;
import de.launchmedevteam.launchme.classes.MinecraftInstance;
import de.launchmedevteam.launchme.classes.Parameter;
import de.launchmedevteam.launchme.lib.OSInfo;
import de.launchmedevteam.launchme.lib.OSInfo.OS;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.LogOutputStream;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.io.FileUtils;
import org.json.JSONException;

public class MinecraftProcessBuilder {
	ArrayList<Parameter> params = new ArrayList<>();
	ArrayList<Parameter> minecraft_params = new ArrayList<>();
	Process process;
	String startcommand = "";
	File path_of_start_jar;
	MinecraftInstance mi;

	public MinecraftProcessBuilder(MinecraftInstance mi) {
		// Function generates Java Args&starts them in new Thread
		this.mi = mi;
		if (mi.getJVMArgs().isEmpty()) {
			// Adding default arguments..getClass().

		}

		// JSONObject versiondetails = new JSONObject(new
		// String(Files.readAllBytes(Paths.get(dir + "versions/" + version + "/" +
		// version + ".json"))));
		// "-Xmx2G -XX:+UnlockExperimentalVMOptions -XX:+UseG1GC -XX:G1NewSizePercent=20
		// -XX:G1ReservePercent=20 -XX:MaxGCPauseMillis=50 -XX:G1HeapRegionSize=32M"

		if (mi.getVersion().getRaw().has("jar")) {
			// Dont have own jar file
			try {
				path_of_start_jar = new File(Main.settings.getMinecraftPath() + "groups/" + mi.getGroup().getID()
						+ "/versions/" + mi.getVersion().getRaw().getString("jar") + "/"
						+ mi.getVersion().getRaw().getString("jar") + ".jar");

				if (!path_of_start_jar.exists())
					path_of_start_jar = new File(Main.settings.getMinecraftPath() + "gloabl/versions/"
							+ mi.getVersion().getRaw().getString("jar") + "/"
							+ mi.getVersion().getRaw().getString("jar") + ".jar");
				if (!path_of_start_jar.exists()) {
					// Must download version...

					// For now: skipping!
					Logger.log(6, "External version jar file not found! Skipping");
					return;
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			// Have have own jar file
			try {
				path_of_start_jar = new File(Main.settings.getMinecraftPath() + "groups/" + mi.getGroup().getID()
						+ "/versions/" + mi.getVersion().getRaw().getString("id") + "/"
						+ mi.getVersion().getRaw().getString("id") + ".jar");

				if (!path_of_start_jar.exists()) {
					Logger.log(5, "No specific jar! Using general!");
					path_of_start_jar = new File(Main.settings.getMinecraftPath() + "global/versions/"
							+ mi.getVersion().getRaw().getString("id") + "/" + mi.getVersion().getRaw().getString("id")
							+ ".jar");

				}
				if (!path_of_start_jar.exists()) {
					// Must download version...

					// For now: skipping!
					Logger.log(6, "Version jar file not found! Skipping");
					return;
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		if (OSInfo.getOs() == OS.WINDOWS)
			params.add(new Parameter("XX:HeapDumpPath",
					"MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump"));

		// settempdir
		generateTempDir();

		params.add(new Parameter("Djava.library.path", getTempDir()));
		params.add(new Parameter("Dminecraft.launcher.brand", "launchme"));
		params.add(new Parameter("Dminecraft.launcher.version", Main.version));
		params.add(new Parameter("Dminecraft.client.jar", path_of_start_jar.getAbsolutePath()));

		params.add(new Parameter("Duser.language", Main.lang.getCode()));

		ArrayList<Library> libs = mi.getVersion().getAllLibs();
		String charseprator = ":";
		if (OSInfo.getOs() == OSInfo.OS.WINDOWS)
			charseprator = ";";
		Parameter p = new Parameter("cp", "", " ", false);

		for (int i = 0; i < libs.size(); i++) {
			if (libs.get(i).getAbsolutFilePath() != null)
				p.pushValue(libs.get(i).getAbsolutFilePath(), charseprator);
		}
		p.pushValue(path_of_start_jar.getAbsolutePath(), charseprator);
		params.add(p);

		/*
		 * if(versiondetails.has("logging")) { startcommand += " " +
		 * versiondetails.getJSONObject("logging").getJSONObject("client").getString(
		 * "argument").replace("${path}", "\"" + dir + "logging/" +
		 * versiondetails.getJSONObject("logging").getJSONObject("client").getJSONObject
		 * ("file").getString("id") + "\"");
		 * System.out.println(versiondetails.getJSONObject("logging").getJSONObject(
		 * "client").getString("argument").replace("${path}", "\"" + dir + "logging/" +
		 * versiondetails.getJSONObject("logging").getJSONObject("client").getJSONObject
		 * ("file").getString("id")) + "\""); }
		 */
		if (mi.getAdditionalParameters().size() == 0) {
			// No Args set!
			// Adding default Parameters
			params.add(new Parameter("Xmx", Main.settings.getDefaultRam() + "M", "", false));
			params.add(new Parameter("XX:+UnlockExperimentalVMOptions", null));
			params.add(new Parameter("XX:+UseG1GC", null));
			params.add(new Parameter("XX:G1NewSizePercent", "20", "=", false));
			params.add(new Parameter("XX:G1ReservePercent", "20", "=", false));
			params.add(new Parameter("XX:MaxGCPauseMillis", "50", "=", false));
			params.add(new Parameter("XX:G1HeapRegionSize", "32M", "=", false));
		} else
			params.addAll(mi.getAdditionalParameters());

		String rawArgs[] = mi.getVersion().getMinecraftArguments().split("--");
		for (int i = 1; i < rawArgs.length; i++) {
			String ss[] = rawArgs[i].split("\\s+", 2);
			Parameter mp = new Parameter(ss[0], ss[1], " ");
			switch (mp.getValue()) {
			case "${auth_player_name} ":
				mp.setValue(mi.getAccount().getUserName());
				break;
			case "${version_type} ":
				mp.setValue(mi.getVersion().getType());
				break;
			case "${version_name} ":
				mp.setValue(mi.getVersion().getVersionName());
				break;
			case "${game_directory} ":
				mp.setValue(Main.settings.getMinecraftPath() + "groups/" + mi.getGroup().getID() + "/workdir");
				break;
			case "${assets_root} ":
				mp.setValue(Main.settings.getMinecraftPath() + "global/assets");
				break;
			case "${assets_index_name} ":
				mp.setValue(mi.getVersion().getAssetsIndex());
				break;
			case "${auth_uuid} ":
				mp.setValue(mi.getAccount().getUUID().toString());
				break;
			case "${auth_access_token} ":
				mp.setValue(mi.getAccount().getAccessToken());
				break;
			case "${user_properties} ":
				mp.setValue("{}");
				break;
			case "${user_type} ":
				mp.setValue(mi.getAccount().getAccountType());
				break;
			}

			minecraft_params.add(mp);

		}
		if (mi.getProfile().getResolution() != null) {
			// Custom Resolution is set. Adding to Arguments
			minecraft_params
					.add(new Parameter("width", String.valueOf(mi.getProfile().getResolution().getWidth()), " "));
			minecraft_params
					.add(new Parameter("height", String.valueOf(mi.getProfile().getResolution().getHeight()), " "));
		}

		startcommand = Main.java_start;
		for (int i = 0; i < params.size(); i++) {
			// Generate String
			startcommand += " -" + params.get(i).getName() + params.get(i).getDelimiter();
			if (params.get(i).needStringChar())
				startcommand += "\"";
			startcommand += params.get(i).getValue();
			if (params.get(i).needStringChar())
				startcommand += "\"";
		}
		startcommand += " \"" + mi.getVersion().getMainClass() + "\"";

		for (int i = 0; i < minecraft_params.size(); i++) {
			// Generate String
			startcommand += " --" + minecraft_params.get(i).getName() + minecraft_params.get(i).getDelimiter() + "\""
					+ minecraft_params.get(i).getValue() + "\"";
		}
		startcommand = startcommand.replaceAll("/", Matcher.quoteReplacement(File.separator));
		Logger.log(8, "Starting Minecraft...");
		System.out.println(startcommand);
		new Thread(new Runnable() {
			@Override
			public void run() {
				

			    try {
			        LogOutputStream out = new LogOutputStream() {

			            @Override
			            protected void processLine(String line, int level) {
			                System.out.println(line);
			            }
			        };
			        LogOutputStream err = new LogOutputStream() {

			            @Override
			            protected void processLine(String line, int level) {
			                System.err.println(line);
			            }
			        };
			        PumpStreamHandler psh = new PumpStreamHandler(out, err);
			        CommandLine cl = CommandLine.parse(startcommand);
			        DefaultExecutor exec = new DefaultExecutor();
					File dir = new File(
							Main.settings.getMinecraftPath() + "groups/" + mi.getGroup().getID() + "/workdir");
			        exec.setWorkingDirectory(dir);
			        exec.setExitValues(new int[] { 0, 1 ,137}); //0 = All ok; 1 = Crashed; 137 = Killed TaskManager
			        exec.setStreamHandler(psh);
			        int code = exec.execute(cl);
					if (code != 0&&code != 137) 
						killed();
					else
						exited();
				
					deleteTempDir();
			    } catch (Exception e) {
			        }

			}

		}).start();

	}

	private void killed() {
		// TODO Auto-generated method stub

	}

	private void exited() {
		// TODO Auto-generated method stub

	}

	private void generateTempDir() {
		Logger.log(4, "TEMPDIR: " + getTempDir());
		new File(getTempDir()).mkdirs();
	}

	private String getTempDir() {
		return System.getProperty("java.io.tmpdir") + "/launchme/minecraft/" + mi.getTempDir().toString();
	}

	private void deleteTempDir() {
		Logger.log(5, "Deleting temp dir!");
		try {
			FileUtils.deleteDirectory(new File(getTempDir()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
