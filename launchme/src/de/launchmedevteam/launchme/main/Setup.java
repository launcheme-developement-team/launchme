package de.launchmedevteam.launchme.main;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.launchmedevteam.launchme.classes.LauncherSettings;

public class Setup {
	JSONObject settings = null;
	public Setup() {
		//Checking if launcheme app dir is created
		File app_dir_folder = new File(Main.application_data_path);
		if(app_dir_folder.exists() && app_dir_folder.isDirectory()) 
			Logger.debug("LaunchME folder exists");
		else {
			Logger.setup("Welcome to LaunchME"); //Probably a new installation
			app_dir_folder.mkdirs();
		}
		
		
		
		File settings_file = new File(Main.application_data_path + "settings.json");
		if(settings_file.exists()) {
            try {
				Logger.log(8, "Found a settings.json file. Loading it...");
				settings = new JSONObject(new String(Files.readAllBytes(Paths.get(settings_file.getAbsolutePath()))));


				
				//if Setup success run step 2
				step2();
			} catch (JSONException | IOException e) {
				Logger.log(6, "settings.json file corrupted. Running setup!");
				//run setup assistent
				e.printStackTrace();
			}
		}else {
			settings = new JSONObject();
			//run setup assistent
			Logger.log(6, "settings.json does not exists! Running setup");
		}
	}
	
	public void exitSetup(boolean success) {
		if(success) {
			//Start GUI
		}
		else
			Main.exit(2);
	}
	
	public void step2() {

		//Settings file loaded or created
		//Checking if everything is correct

		Main.settings = new LauncherSettings(settings);
		
		
		if(Main.settings.check()) {
			//All Settings correct. Exiting...
			Logger.log(5, "settings.json file is valid!");
			finishSetup();
			return;
		}else {
			Logger.log(6, "settings.json file corrupted / not up-to-date. Checking for errors and running setup.");
		}
		
		JSONArray file_errors = Main.settings.getErrors();
		
	}
	
	public void finishSetup() {
		//Setup completed. Checking and creating folder structure!
		
		
		
		ArrayList<String> allFolders = new ArrayList<String>();

		allFolders.add(""); //Root path of launcher
		allFolders.add("global/");
		
		allFolders.add("global/assets/");
		
		allFolders.add("global/downloads/");
		allFolders.add("global/downloads/json/");
		allFolders.add("global/downloads/launchme/");
		allFolders.add("global/downloads/libraries/");
		allFolders.add("global/downloads/mods/");
		allFolders.add("global/downloads/temp/");
		
		allFolders.add("global/minecraft/");
		allFolders.add("global/minecraft/resourcepacks/");
		
		allFolders.add("global/screenshots/");
		
		allFolders.add("global/versions/");

		File folder;
		boolean needToCorrectPaths = false;
		for(int i = 0; i < allFolders.size(); i++) {
			folder = new File(Main.settings.getMinecraftPath() + allFolders.get(i));
			if(!folder.exists()) {
				needToCorrectPaths = true;
				folder.mkdirs();
			}
		}
		if(needToCorrectPaths) {
			//Folders were modified!
			Logger.setup("Folder structure was modified!");
		}
	}
}
