package de.launchmedevteam.launchme.main;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.launchmedevteam.launchme.lib.Hash;


public class MinecraftRepository {
	static JSONObject versionindex = new JSONObject();
	static boolean suc = false;
	
	
	public static void refresh() {
		getVersionIndex();
		try {
			versionindex = new JSONObject(new String(Files.readAllBytes(Paths.get(Main.settings.getMinecraftPath() + "global/downloads/temp/version_manifest.json"))));

			Logger.info("Temporary Versionindex initialized: " + versionindex.getJSONArray("versions").length() + " total");
			suc = true;
		} catch (JSONException | IOException e) {
			//No Ethernet available. Critical Error!
			//No official Versions saved(indexes)
			//Trying to disable repo = Only use local Versions!
			e.printStackTrace();
		}
	}
	
	public static void verifyAssets(String version) {
		//Load Assets index
		try {
			JSONObject assets = new JSONObject(new String(Files.readAllBytes(Paths.get(Main.settings.getMinecraftPath() + "global/assets/indexes/" + version + ".json"))));
			boolean virtual = false;
			if(assets.has("virtual") && assets.getBoolean("virtual")) {
				//Old Minecraft Assets: Need to use "virtual/legarcy" folder!
				virtual = true;
			}
			JSONArray index = assets.getJSONArray("objects");
			for(int i = 0; i < index.length(); i++) {
				JSONObject asset = index.getJSONObject(i);
				Logger.debug("Downloading:" + asset.getString("hash"));
				File f = new File(Main.settings.getMinecraftPath() + "global/assets/objects/" + asset.getString("hash").substring(0,2) + "/" + asset.getString("hash"));
				if(!f.exists() || f.isDirectory() || f.length() != asset.getInt("size") || !asset.getString("hash").equalsIgnoreCase(Hash.createSha1(f))) {
					//File does not exist or is corrupted
					Logger.minecraft("Downloading: " + asset.getString("hash"));
					FileUtils.copyURLToFile(new URL("http://resources.download.minecraft.net/" + asset.getString("hash").substring(0, 2) + "/" +asset.getString("hash")), f);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		
	}
	
	public static void downloadVersionJSON(JSONObject index) {
		try {
			Logger.debug("Downloading index:" + index.getString("id"));
				File f = new File(Main.settings.getMinecraftPath() + "global/versions/" + index.getString("id") + "/" + index.getString("id") + ".json");
				if(!f.exists() || f.isDirectory()) {
					//File does not exists
					FileUtils.copyURLToFile(new URL(index.getString("url")), f);
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	//ToDo: Download Assets Index
	
	public static void download(int id) {
		try {
			JSONObject v = versionindex.getJSONArray("versions").getJSONObject(id);
			Logger.debug(v.toString());
			Logger.info("Downloading version: " + v.getString("id") + "...");
			

			URL url = new URL(v.getString("url"));
			File file = new File(Main.settings.getMinecraftPath() + "global/versions/" + v.getString("id") + "/" + v.getString("id") + ".json");
			FileUtils.copyURLToFile(url, file);
			
			//ToDo: Assets Index, and veriyAssets(), and old Assets(legarcy)
			
			
			
		} catch (JSONException | IOException e) {
			e.printStackTrace();
		}
	}
	
	public static File getVersionIndex() {
		File file = null;
		try {
			URL url = new URL("https://launchermeta.mojang.com/mc/game/version_manifest.json");
			file = new File(Main.settings.getMinecraftPath() + "global/downloads/temp/version_manifest.json");
			FileUtils.copyURLToFile(url, file);
			Logger.info("Versionindex downloaded and saved");
			
		} catch (IOException e) {
			//No Server Connection. 
			Main.offlinemode = true;
			Logger.warn("No Ethernet Connection!");
			if(Main.debug)
				e.printStackTrace();
		}
		return file;
	}
	

}
