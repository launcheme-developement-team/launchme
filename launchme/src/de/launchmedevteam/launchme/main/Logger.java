package de.launchmedevteam.launchme.main;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Logger {
	public static String current_log_file;
	private static boolean log_to_file = false;

	public Logger() throws IOException {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		current_log_file = formatter.format(new Date());
		List<String> lines = Arrays.asList();
		Path file = Paths.get("logs/" + current_log_file + ".log");
		Files.write(file, lines, Charset.forName("UTF-8"));
		log_to_file = true;

	}

	public static void log(int type, String message) {
		if (!Main.debug && type == 4)
			return;
		/*
		 * Types: 2 = LOGIN 3 = LOGOUT 4 = DEBUG 5 = INFO 6 = WARN 7 = CRITICAL 8 =
		 * SETUP CRITICAL = SERVER STOP
		 */
		String date = new SimpleDateFormat("yyyy-mm-dd_hh-mm-ss").format(new Date());
		String type1 = "";
		switch (type) {
		case 2:
			type1 = "LOGIN";
			break;
		case 3:
			type1 = "LOGOUT";
			break;
		case 4:
			type1 = "DEBUG";
			break;
		case 5:
			type1 = "INFO";
			break;
		case 6:
			type1 = "WARN";
			break;
		case 7:
			type1 = "CRITICAL";
			break;
		case 8:
			type1 = "SETUP";
			break;
		case 9:
			type1 = "MINECRAFT";
			break;
		default:
			type1 = "OTHER";
			break;
		}
		String log_message = "[" + type1 + "] [" + date + "] " + message;

		if (log_to_file) {
			PrintWriter log = null;
			try {
				log = new PrintWriter(new FileWriter("logs/" + current_log_file + ".log", true));
			} catch (IOException e) {
				System.out.println("[WARM] Error writing Log to file!");
				e.printStackTrace();
			}
			log.println(log_message);
			log.close();
		}

		System.out.println(log_message);

		if (type == 7) {
			Main.exit(1);
		}
	}

	public static void login(String msg) {
		log(2, msg);
	}

	public static void logout(String msg) {
		log(3, msg);
	}

	public static void debug(String msg) {
		log(4, msg);
	}

	public static void info(String msg) {
		log(5, msg);
	}

	public static void warn(String msg) {
		log(6, msg);
	}

	public static void critical(String msg) {
		log(7, msg);
	}

	public static void setup(String msg) {
		log(8, msg);
	}

	public static void minecraft(String msg) {
		log(9, msg);
	}

	public static void other(String msg) {
		log(10, msg);
	}

}
