package de.launchmedevteam.launchme.main;


import de.launchmedevteam.launchme.classes.Language;
import de.launchmedevteam.launchme.classes.LauncherSettings;
import de.launchmedevteam.launchme.lib.OSInfo;
 
public class Main {
	public static boolean debug = true;
	public static String version = "0.1.4";
	public static String os;
	public static boolean offlinemode = true; 
	public static String java_start = System.getProperty("java.home") + "/bin/java";
	public static LauncherSettings settings;
	public static String application_data_path = System.getProperty("user.home")  + "/.launchme/";
	public static Setup setup;
	public static Language lang = new Language();
	 
	
    public static void main(String[] args){
    	Logger.log(5, "Starting Launcher v" + version + "...");
    	Logger.log(5, "Parsing arguments...");
    	Console.parseArguments(args);

    	if(debug)
    		Logger.log(4, "Debug mode enabled!"); 
    	Logger.log(5, "Initialize...");
    	
    	
    	switch(OSInfo.getOs()) {
		case MAC:
			os = "osx";
			break;
		case WINDOWS:
			os = "windows";
			Main.application_data_path =  System.getProperty("user.home") + "/AppData/Roaming/launchme/";
			java_start = "javaw";
			break;
		default:
			os = "linux";
			break;
	}
    	Logger.log(4, application_data_path);
	Logger.log(5, "Detected OS: " + os + " x" +  System.getProperty("sun.arch.data.model"));
    	
	Runtime.getRuntime().addShutdownHook(new Thread() {
        public void run() {
				Logger.log(5, "Exit...");
        }
    });
	
	
	

		Logger.log(5, "Running Setup...");
		setup = new Setup();
		

    	Logger.log(5, "Adding Accounts...");
    	
    	 Logger.info("Downloading Minecraft version index...");
    	 MinecraftRepository.refresh();
    	 MinecraftRepository.download(10);
    	
	
 
    }
	
	public static void exit(int status) {
		System.exit(status);
		
	}
	

}
