package de.launchmedevteam.launchme.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import de.launchmedevteam.launchme.classes.StartArgument;
 
public class Console {
	public Console(){
		Thread thread = new Thread(){
		    public void run(){

	    	       try {
		    	
		    	BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		    	String line = "";

		    	   while (true) {
		    		   line = in.readLine();
		    		  
		    		   if(line.equalsIgnoreCase("stop")||line.equalsIgnoreCase("exit"))
		    			  Main.exit(0);
						
		    	   }
		    	   
	    	       } catch (IOException e) {
						e.printStackTrace();
					}


		    }
		    
		};
		thread.start();
		
	}
	
	
	public static void parseArguments(String args[]) {
        for(String arg_text : args){
        	StartArgument arg = new StartArgument(arg_text);
        	switch(arg.getType()){
        	case StartArgument.DEBUG:
        		//Debug mode:
        		Main.debug = Boolean.parseBoolean(arg.getValue().toString());
        	}
        }
	}
	

}
